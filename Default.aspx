﻿<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.vb" Inherits="Joseline._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

  <header class="masthead text-center text-white">
    <div class="masthead-content">
      <div class="container">
        <h1 class="masthead-heading mb-0">Aplicación ASP.NET</h1>
        <h2 class="masthead-subheading mb-0">Prueba de Conexion SQL</h2>
          <asp:Button class="btn btn-primary btn-xl rounded-pill mt-5" ID="btn_test" runat="server" Text="Conectar" /><br><br>
         <h2> <asp:Label ID="lbl_status" runat="server" Text="Esperando conexion.."></asp:Label></h2>
      </div>
    </div>
    <div class="bg-circle-1 bg-circle"></div>
    <div class="bg-circle-2 bg-circle"></div>
    <div class="bg-circle-3 bg-circle"></div>
    <div class="bg-circle-4 bg-circle"></div>
  </header>


</asp:Content>
